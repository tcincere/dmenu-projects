#!/bin/bash

fd_location="~/.local/bin/fd"
MENU="rofi -dmenu -i -p Search"
# KITTY_PATH=$(which kitty)
WEZTERM_PATH=$(which wezterm)
HELIX_PATH=$(which hx)
# EDIT="${KITTY_PATH} -e ${HELIX_PATH}"
EDIT="${WEZTERM_PATH} start -- ${HELIX_PATH} $1"
